#[macro_use]
extern crate static_fir;
extern crate libpulse_binding as pulse;
extern crate libpulse_simple_binding as psimple;

mod fir;

use fir::*;

use std::io::{self, Write};

use static_fir::FirFilter;
use psimple::Simple;
use pulse::sample::{Spec, SAMPLE_S16NE};
use pulse::def::BufferAttr;
use pulse::stream::Direction;

const BUFFER_SIZE: u32 = 1024;

fn sigmoid(x: f32) -> f32 {
    1.0 / (1.0 + (-x).exp())
}

fn main() {
    let spec = Spec {
        format: SAMPLE_S16NE,
        channels: 2,
        rate: 44100,
    };
    assert!(spec.is_valid());

    let attr = BufferAttr {
        fragsize: BUFFER_SIZE,
        maxlength: BUFFER_SIZE,
        tlength: BUFFER_SIZE,
        prebuf: BUFFER_SIZE,
        minreq: BUFFER_SIZE,
    };

    let s = Simple::new(
        None,                // Use the default server
        "FooApp",            // Our application’s name
        Direction::Record,   // We want a record stream
        None,                // Use the default device
        "Music",             // Description of our stream
        &spec,               // Our sample format
        None,                // Use default channel map
        Some(&attr)          // Use default buffering attributes
    ).unwrap();

    let mut fir = FirFilter::<LowpassFir4>::new();

    let mut data = [0i16; BUFFER_SIZE as usize];
    let stdout = io::stdout();
    let mut handle = stdout.lock();

    loop {
        unsafe {
            let ptr = data.as_mut_ptr() as *mut u8;
            let len = data.len() * 2;
            s.read(std::slice::from_raw_parts_mut(ptr, len)).unwrap();
        }

        let rms = 5.0 * (
            data.iter().map(|x| {
                let x = *x as f32;
                x * x
            }).sum::<f32>()
            /
            BUFFER_SIZE as f32
        ).sqrt() / std::i16::MAX as f32;

        let vu = 20.0 * rms.log10();
        let sig = sigmoid(0.2 * vu);

        // gamma correction
        let sig = sig.powf(2.8);

        // constant background
        let back = 0.0;
        let sig = (1.0 - back) * sig + back;

        // filtering
        let sig = fir.feed(sig).max(0.0).min(1.0);

        // quantization
        let q = (256.0 * sig).floor() as u8;
        eprintln!("got q {:?}", q);

        // invert pwm (active low)
        let q = 255 - q;

        // frame
        let q = q.min(0xfe);
        let frame = [0xff, q, q, q, q];

        // write
        handle.write(&frame).unwrap();
        handle.flush().unwrap();
    }
}
