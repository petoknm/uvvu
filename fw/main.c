#include <stm8s.h>

// PWM
static uint8_t pwm[4];

void pwm_set(uint8_t new[4]) {
  for (uint8_t i=0; i<4; i++) pwm[i] = new[i];
}

void pwm_run() {
  PC_DDR |= 0xF0;
  PC_CR1 |= 0xF0;
  PC_CR2 &= ~(0xF0);

  uint8_t i = 0;
  while (1) {
    for (uint8_t ch=0; ch<4; ch++){
      if (i < pwm[ch]) {
        PC_ODR |= 1 << (4+ch);
      } else {
        PC_ODR &= ~(1 << (4+ch));
      }
    }
    i++;
    for(int i=0; i<10; i++) __asm__("nop");
  }
}

// Serial message framing
#define DELIMITER 0xff
#define MSG_LEN   4

static uint8_t msg[MSG_LEN];
static uint8_t pos = 0;

void framing_rx(uint8_t code) {
  if (code == DELIMITER) pos = 0;
  else msg[pos++] = code;
  if (pos == MSG_LEN) pwm_set(msg);
}

// UART
#define BAUDRATE 115200
#define UART1_CR2_RIEN 5

void uart1_init() {
    uint16_t div = (F_CPU + BAUDRATE / 2) / BAUDRATE;
    UART1_BRR2 = ((div >> 8) & 0xF0) | (div & 0x0F);
    UART1_BRR1 = div >> 4;
    UART1_CR2 |= (1 << UART1_CR2_REN) | (1 << UART1_CR2_RIEN);
}

void uart1_isr() __interrupt(UART1_RXC_ISR) {
    if (UART1_SR & (1 << UART1_SR_RXNE)) framing_rx(UART1_DR);
}

void main() {
    // clock divider = 0 => 16MHz
    CLK_CKDIVR = 0x00;

    uart1_init();
    enable_interrupts();

    pwm_run();
}
